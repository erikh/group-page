# Miscellaneous

---

**Table of Contents**

- [Mounting](#mounting)
- [Looking for standard installations](#looking-for-standard-installations)
- [Various nice commands and procedures](#various-nice-commands-and-procedures)

---

## Mounting

**Example on aurora (cluster in lund):**

Make sure that you are in directory "/home/erikh/aurora" or equivalent on you computer)
```
sshfs aurora:/lunarc/nobackup/users/erikh /home/erikh/aurora/
```
The first dir is the target to mount to, the second is your local directory.

## Looking for standard installations

Sometimes it has handy to look for installed packages. This can be done with (here hdf5 is used as an example)
```
rpm -ql hdf5
```
will look for hdf5 files

## Various nice commands and procedures

**Operations on files**
* Copy full name (path/name) of the target files into a gile e.g. called "list.txt" 
* if you want to delete the files, use 
```
for i in `cat list`; do rm "$i" ; done
```
* if you want to make a backup, use 
```
for i in `cat list`; do cp "$i" "$i".bak ; done
```

**vi editor**
* Copy and paste problems (indentation problems). Open vi and type ```:set paste```
