# Compilers and MKL

---

**Table of Contents**

- [MKL](#mkl)
- [gfortran](#gfortran)
- [Profiling](#profiling)

---

## MKL

Test...Here goes tmole description...

## gfortran 

### Example from gfortran version gcc-5.4.0

* Download gcc tar.gz file https://ftp.gnu.org/gnu/gcc/
* Configure and install according (the "make" step can take several hours)
```
tar xzf gcc-5.4.0.tar.gz
cd gcc-4.6.2
./contrib/download_prerequisites
cd ..
mkdir objdir
cd objdir
$PWD/../gcc-5.4.0/configure --prefix=$HOME/gcc-5.4.0 --enable-languages=c,c++,fortran,go
make
make install
```
* Remember to configure the LD_LIBRARY_PATH for instance as (either in basrc or prefereble in a file "gcc-5.4.0.sh")
```
export GFORTRAN=/home/erik/gcc-5.4.0/bin
PATH=$HOME/bin:$GFORTRAN:$PATH
if [ -z "$LD_LIBRARY_PATH" ]; then
   LD_LIBRARY_PATH="/home/erik/gcc-5.4.0/lib/../lib64"
 else
   LD_LIBRARY_PATH="/home/erik/gcc-5.4.0/lib/../lib64:$LD_LIBRARY_PATH"
fi
export LD_LIBRARY_PATH
export PATH
```
* gfortran can then be activated by ```source gcc-5.4.0.sh```

## Profiling

### gprof in DALTON and DIRAC

The gprof profiler comes with usual Linux distributions. DALTON and DIRAC needs to by called with certain compiler flags to use the profiler

./setup --extra-fc-flags=-g -pg --extra-cc-flags=-g -pg --extra-cxx-flags=-g -pg type=release build-profile

Run dalton as:
$DALTON/dalton -t <SRACTCH> -pg -D -M 1024 -N 1 -o dalton.out dal.dal mol.mol

- the results can be found in the <SCRATCH> which we do not delete (-D option)

Alternatively, run DALTON with 

$DALTON/dalton -t <SRACTCH> -pg -D -M 1024 -N 1 -o dalton.out dal.dal mol.mol

gprof2dot -f prof PROFILE | dot -Tpng | display

### vtune in DALTON and DIRAC

vtune follows with the Intel oneAPI Base Toolkit (install it from [here](https://www.intel.com/content/www/us/en/develop/documentation/vtune-install-guide-linux/top.html)

If you installed it as root (sudo): ```. /opt/intel/oneapi/setvars.sh```

If you installed it as user: ```. ~/intel/oneapi/setvars.sh```

for dalton use

export DALTON_LAUNCHER='vtune -collect hotspots -r /path-tp-results'
vtune -report gprof-cc -result-dir /path-tp-results -format text -report-output profile_gprof_cc.txt
gprof2dot -f axe profile_gprof_cc.txt | dot -Tpng -o profile_call_graph.png


