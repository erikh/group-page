# QM/MM protocols

---

**Table of Contents**

- [Job submission examples](#job-submission-examples)

---

## Job submission examples

### Basis set study with dependencies (made for the DIRAC)


```
for basis in cc-pVDZ cc-pVTZ cc-pVQZ # 3 basis set to be investigated...
  do
  echo "$basis"
  mkdir -p $basis
  cd $basis
  # copy required filres to run-dir
  pwd
  cp ../blyp.inp .
  cp ../blyp_cpp_*.inp .
  cp ../run.sh .
  cp ../run*.sh .
  cp ../merc.xyz .
  sed -i 's/basis_set/'$basis'/g' blyp.inp # requies file blyp.inp (see below)
  sbatch run.sh > tmp
  for i in {1..8}
  do
      echo ${i}
      sed -i 's/basis_set/'$basis'/g' blyp_cpp_${i}.inp
      awk '{print $4}' tmp > jobid
      sbatch --dependency=afterok:`cat jobid` run_${i}.sh
  done
  rm tmp
  cd ..
done
```
