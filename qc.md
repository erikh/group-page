# Quantum mechanical software

---

**Table of Contents**

- [Turbomole](#turbomole)
- [DALTON](#dalton)
- [DIRAC](#dirac)
- [MOLCAS](#molcas)
- [QCMaquis stand-alone](#qcmaquis-stand-alone)

---

## Turbomole

- [Structure optimization](#structure-optimization)
- [Hessian](#hessian)

### Structure optimization 

1. Obtain a coord file
2. Copy coord file and the scripts ```define.sh``` (here: [define](#define)) and ```run-define.sh``` (here [run-define](#run-define)) to a the directory you wish to run the calculations in..
3. Modify ```define.sh``` according to what is needed (change e.g. charge of molecule, open-shell?, if open-shell, what spin?)
2. Run ```define.sh``` (```chmod u+x define.sh``` and ```./define.sh```)


#### define ####
*** define.sh  ***
```
#!/bin/csh

foreach complex (resting_state)
   if ($complex == resting_state) then
   cp tpssd3_$complex.bohr coord 
define <<EOF
define <<EOF


a coord
*
no
bb
all def2-SV(P)
*
eht
y
y
2
y

dft
on
func
tpss
*
ri
on
*
dsp
on
bj
*
*
EOF

     mkdir -p $complex
     mv alpha $complex
     mv beta $complex
     mv mos $complex
     mv basis $complex
     mv auxbasis $complex
     mv control $complex
     mv coord $complex
     cp run-define.sh $complex
   endif
end
```

#### run define ####
***run-define.sh***
```
#!/bin/bash

cp control control.tmp
cat control.tmp | sed -e 's/$scfiterlimit       30/$scfiterlimit       300/' > control.tmp2       
cat control.tmp2 | sed -e 's/$scforbitalshift  closedshell=.05/$scforbitalshift  closedshell=.25/' > control        
rm control.tmp && rm control.tmp2
#awk '!/amat file=r/' control > temp && mv -f temp control

array=(
tpss
)
for i in "${array[@]}"
do
   echo '#!/bin/bash'                       > run_$i.sh
   echo '#SBATCH -N 1'                     >> run_$i.sh 
   echo '#SBATCH -n 8'                     >> run_$i.sh
   echo '#SBATCH -t 168:00:00'             >> run_$i.sh
   echo '#SBATCH -J lpmo_bde'              >> run_$i.sh

   echo  'export PARA_ARCH=MPI'            >> run_$i.sh 
   echo  'TURBODIR=/lunarc/nobackup/projects/bio/TURBO/Turbo7.1'        >> run_$i.sh 
   echo  'PATH=$TURBODIR/bin/x86_64-unknown-linux-gnu_mpi:$TURBODIR/scripts:$PATH'  >> run_$i.sh 
   echo  'export PARNODES=8'               >> run_$i.sh
   echo  'export PATH'                     >> run_$i.sh
        
   echo  'cd $SNIC_TMP'                   >> run_$i.sh 
   echo  'cp -p $SLURM_SUBMIT_DIR/* .'    >> run_$i.sh 
   echo  'jobex -backup -c 400 -ri'       >> run_$i.sh 
   echo  'cp -pu * $SLURM_SUBMIT_DIR'     >> run_$i.sh 
   
   chmod u+x run_$i.sh
   sbatch run_$i.sh 
done```

### Hessian

Calculation of the (molecular) Hessian requires a well-converged structure. Start [here](#structure-optimization) if you don't have a structure
. After you have got a good starting structure, use the the turbomole program aoforce to calculate the  scripts:
1. Run the script [aoforce-1.sh](#aoforce-1) which converges the structure with tighter convergence criteria
2. Run the script [aoforce-2.sh](#aoforce-2) which uses the program aoforce to calculate the Hessian

#### aoforce 1
***aoforce-1.sh***
```
#!/bin/csh

foreach complex (resting_state)
        if ($complex == "resting_state") then
           cd $complex
             rm -rf aoforce
             mkdir -p aoforce
             cd aoforce
                cp ../* .
                rm nextstep
                sed -i 's/$scfconv   6/$scfconv   8/g' control
                sed -i 's/jobex -backup -c 400 -ri/jobex -backup -c 400 -ri -gcart 4/g' run_tpss.sh
                sbatch run_tpss.sh
             cd ..
           cd ..
        endif
end 
```

#### aoforce 2
***aoforce-2.sh***
```
#!/bin/csh
foreach complex (resting_state)
        if ($complex == "resting_state") then
           cd $complex
             cd aoforce
                turbofreq
                sed -i 's/jobex -backup -c 400 -ri -gcart 4/aoforce > logf/g' run_tpss.sh
                sed -i 's/$maxcor 700/$maxcor 2500/g' control
                sbatch run_tpss.sh
             cd ..
           cd ..
        endif
end
```

### Broken symmetry calculations
For a simple case (triplet to open-shell singlet), converge the triplet state and start for the converged orbitals. In this case out triplet has 128 alpha electrons and 126 beta electrons. 
Then change alpha and beta electrons to 127 (find the strip below in the control file):
```
$alpha shells a       1-128=>127                             
$beta shells  a       1-126=>127
```

---

## DALTON

### Install

* General sequential install
```./setup build_name```

Parallel installations depend a bit on the cluster. A few examples are given below

#### On abacus

* Parallel installation (OpenMPI / MKL)
```module load module load GCC/7.3.0-2.30 OpenMPI/3.1.1 iomkl/2018b CMake/3.12.1```
```./setup --mpi --mkl=sequential build_name```

#### On abacus

* Parallel installation (intel)
```module load cmake/3.9.4 intel/2018.05 intelmpi/2018.05```
```./setup --fc=mpiifort --cc=mpiicc --cxx=mpiicpc --mkl=sequential build_name```


### Running DALTON 

Below different examples on running dalton are shown

#### Optical rotation (ORD) and electric circular dichroism (ECD)

Here is a small tutorial for running ORD and ECD calculations in DALTON (and also DIRAC)

* Input example of ORD and ECD calculation, using damped response theory

* copy the example below into a file "input.dal" 

```
BASIS
aug-cc-pVDZ
Methyloxirane (B3LYP/6-31G* optimized geometry)
================================================
Atomtypes=3 Generators=0 Angstrom
Charge=6.0 Atoms=3
C       -1.045082    0.615240   -0.060290
C        0.152118   -0.037513    0.489147
C        1.511521    0.100482   -0.148726
Charge=8.0 Atoms=1
O       -0.827106   -0.789457   -0.242703
Charge=1.0 Atoms=6
H       -1.875126    0.879897    0.595964
H       -0.958595    1.221760   -0.963024
H        0.155847   -0.255258    1.560108
H        1.416164    0.337467   -1.213207
H        2.077966   -0.833346   -0.056439
H        2.089255    0.895882    0.337436
#end of input

**DALTON INPUT
.RUN PROPERTIES
.DIRECT
**WAVE FUNCTION
.DFT
 B3LYP
**PROPERTIES
.OPTROT
*ABALNR
.DAMPING
 0.0045566
.FREQ INTERVAL
 0.18 0.28 0.0025
*END OF```

* run ```<path-to-dalton>/dalton -d -nobackup -noarch -M 1024 -N 1 -o output.log input.dal```
* For running on aurora, see below


#### Aurora

* DALTON is installed on aurora. A runscript example is given below

#!/bin/sh
#SBATCH -N 1
#SBATCH --tasks-per-node=8
#SBATCH -t 00:10:00
#SBATCH --qos=test

source /home/erikh/programs/.dalton-ompi-master

cd $SNIC_TMP
cp -p $SLURM_SUBMIT_DIR/* .
$DALTON/dalton -d -nobackup -M 4000 -N 8 -o output.out input.dal
cp -pu * $SLURM_SUBMIT_DIR

### Localized orbitals

To transform to localized orbitals in first iteration, make a specialized DALTON: Find the section in (sirinp.F) 

```
*****  PROCESS AND CHECK INPUT DATA *****
IF (DOSCF .OR. DOCI .OR. DOMC) THEN
         IF(MAXMAC.LE.0 .OR.MAXMAC.GT.200) CALL ERRINP( 5,INPERR)
         ! edh modification to get localized virtuals !
         IF(THRGRD.LT.D0.OR.THRGRD.GT.1000)  CALL ERRINP( 7,INPERR)
END IF
```
**OBS:** Do not use this DALTON for anything else than localized orbitals - the insert allows the gradient to be VERY large (because we do not want do any iterations!)

### Generate dalton mol inputs from xyz files

This can be done with openbabel:

```
babel -i xyz input.xyz -o dalmol output.mol # Default basis: 6-31G*
```
Sometimes it is more flexible to employ a atom-specific basis set. This can be fixed by:

```
sed -i '/Charge/ s/$/ Basis=def2-svp/' input.mol
```
Remember to remove the default basis set and replace "BASIS" by "ATOMBASIS" in the mol file. Below is an example script for a molecule:
```
sed -i '/6-31G*/d' mol.mol
sed -i 's/BASIS/ATOMBASIS/' mol.mol
sed -e 's/\<NoSymmetry\>//g' mol.mol > mol_2.mol # removes NoSymm in mol (if symmetry is not wanted, remove this and next 2 lines)
sed -i 's/ Angstrom/Angstrom/' mol_2.mol
mv mol_2.mol mol.mol
sed -i 's/\Charge=25.0 Atoms=1\b/& Basis=aug-cc-pVTZ/' mol.mol
sed -i 's/\Charge=7.0 Atoms=4\b/& Basis=cc-pVTZ/' mol.mol
sed -i 's/\Charge=6.0 Atoms=6\b/& Basis=cc-pVTZ/' mol.mol
sed -i 's/\Charge=1.0 Atoms=10\b/& Basis=cc-pVDZ/' mol.mol
```

### Debugging

Some tips and tricks for debugging

**MCSCF code**
Debugging of the MCSCF code in DALTON can be tricky, but can be helped by checking some of the convergence parameters. 

The optimization scheme in DALTON builds on a second order Taylor expansion, where the predicted energy change for a parameter step **L** is q(**L**) = **gL** + **LHL** (**g** = gradient and **H** = Hessian). The true energy change is E(**L**). Associated with E(**L**) and q(**L**) are a number of convergence parameters that can be used to check for quadratic convergence (which is a pre-requisite for a correct code). Check these convergence parameters, listed below:
* GRDNORM: Gradient norm. Should decrease quadratic (e.g. 0.1, 0.01, 0.0001 etc...). The CI part calculated in routine CIGRAD, and the orbital part in routine ORBGRD.
  * If error is in gradient, locate the high element(s) giving rise to the problem
* RATIO: E(**L**) / q(**L**). Should go (monotimically) towards 1.0 (0.99...) in the optimization (note that in the last step it is usually set to 1.0000 and this therefore does not count). In DALTON this parameters is calculated as routine SIRSTP.
* STPLNG. Step length taken during the optimization. 
* In the first MACRO ITERATION (MACRO 1), check active energy and CIDIAG - the CI diagonal elements should have an absolute value that is lower than the active energy (otherwise the Hessian gets negative elements as CIDIAG is used in the Davidson preconditioning). 
* Look at actual and predicted energy change. They should be similar (almost identical close to convergence)
* STEP control (if strange steps are observed, there might be a problem here)
* Alternatively, use the NR solver (note that it cannot back-step and will stop if there are negative eigenvalues in the Hessian)
  * With .NR ALWAYS solver: 2. order contribution should be exactly one half of 1. order contribution
* Check that reduced Hessian is symmetric (redH") can also be done with p6flag(51) in input
* Things to be dumbed from file can be done with dmplab

**Memory bugs**
* Trace the routine that gives the problem
* More nasty ones, e.g, where inserting print statements makes code fail/run: boil down to exactly what print statement (and where in the code) it goes wrong
If a routine is spotted, go to this and write debug prints (e.g. debug print 1, debug print 2 etc...) - continue with locate spot where it goes wrong
* If inside a loop, is it first time it quits? If not, are there as many loop prints as expected?

**MC-srDFT**
* Compare single determinant with full CI and only exchange (possibly also full HF exchange) using very high mu - here exchange is shifted between lr and sr parts but total energy should be equilvalent. Check convergence

---

## DIRAC

### Install on aurora 

**sequential**
* Clear modules ```module purge```
* Load gfortran newest version and CMake ```module load GCC/7.3.0-2.30 CMake/3.9.1``` 
* In the dirac folder, run setup and follow the instructions ```./setup build_pe-develop```

**parallel**
* Clear modules ```module purge```
* Load modules gfortran, ompi and CMake  ```module load GCC/7.3.0-2.30 OpenMPI/3.1.1 CMake/3.9.1```
* In the dirac folder, run setup with mpi flag. Follow the instructions ```./setup --mpi build_pe-develop_ompi```

### Running on aurora

Example run scripts are given below
```
#!/bin/sh
#SBATCH -N 1
#SBATCH --tasks-per-node=8 # Parallel run. Set to 1 for sequential 
#SBATCH -t 10:00:00

module purge
module load GCC/7.3.0-2.30 OpenMPI/3.1.1 # remove OpenMPI for sequential run

/home/erikh/programs/dirac-aug2018/build_pe-develop_ompi/pam --inp=camb3lyp.inp --mol=cysSH.xyz --mpi=8
# For sequential run: /home/erikh/programs/dirac-aug2018/build_pe-develop_ompi/pam --inp=camb3lyp.inp --mol=cysSH.xyz
```

---

## MOLCAS

### Install MOLCAS

* Download source from >inset repo<
* **Setup (serial):**
    
```./configure # Runs a configure scripts.```
* ** Setup (parallel):** Here with intel; remember to load the required modules.
    
```./configure -parallel -intel```

### Installation of OpenMolcas on kebnekaise

Geting OpenMolcas:
```
git clone https://gitlab.com/Molcas/OpenMolcas.git [directory]```

Go to the molcas dir and do
```git submodule update --init --recursive```

Load modules (intel/mkl):

```
module load ifort/2018.1.163-GCC-6.4.0-2.28 impi/2018.1.163 icc/2018.1.163-GCC-6.4.0-2.28 HDF5/1.10.1 imkl/2018.1.163```

Run CMake

```
FC=ifort CC=icc CXX=icpc && cmake -DHDF5=ON -DOPENMP=ON -DLINALG=MKL ../```

* For DMRG support (CheMPS2), do
```
FC=ifort CC=icc CXX=icpc && cmake -DCMAKE_INSTALL_PREFIX=/pfs/nobackup/home/e/erikh/programs/open-molcas-jan2019/build_chemps2 -DHDF5=ON -DOPENMP=ON -DLINALG=MKL -DCHEMPS2=ON -DCHEMPS2_DIR=/pfs/nobackup/home/e/erikh/programs/CheMPS2/build/CheMPS2``` 

* Note that a path to the CheMPS2 executable is given, assuming CheMPS2 is installed. See separate section for installation of CheMPS2 (below)

### Installation of CheMPS2 on kebnekaise

* Get program (git clone)

* Load modules (intel/mkl - use the same as for MOLCAS above if the programs should work together!):
```
module load ifort/2018.1.163-GCC-6.4.0-2.28 impi/2018.1.163 icc/2018.1.163-GCC-6.4.0-2.28 imkl/2018.1.163 HDF5/1.10.1```

* Go to the ChemMPS2 source folder and make a build directory ```mkdir build```

* Set compiler flags using CMake:
```
export FC=ifort export CC=icc export CXX=icpc && cmake -DMKL=ON -DSHARED_ONLY=on -DCMAKE_INSTALL_PREFIX=/pfs/nobackup/home/e/erikh/programs/CheMPS2/build/install ../```

* Compile ```make -j8``` and Install (perhaps not really required) ```make install```

Put the modules into a file, e.g., example on runscript (through OpenMolcas)

```
source /pfs/nobackup/home/e/erikh/programs/.openmolcas-intel
export CHEMPS2=/pfs/nobackup/home/e/erikh/programs/CheMPS2/build/CheMPS2
export MOLCAS=/pfs/nobackup/home/e/erikh/programs/open-molcas-jan2019/build_chemps2/
PATH=$PATH:$CHEMPS2:$MOLCAS:/home/e/erikh/bin/
export PATH

#OpenMP settings
export OMP_NUM_THREADS=8

pymolcas -f input-dmrg.input```

### MOLCAS in parallel on kebnekaise (with global arrays)

*Global Arrays installation*
* Get global arrays: ```git clone https://github.com/GlobalArrays/ga.git global-array-jan2018```
* Load modules (see installation of MOLCAS)
* Use the autogen script: ```./autogen.sh```
* make build dir: ```mkdir build```
* Configure: remember to set prefix and double precison integers (i8) ```./configure --enable-i8 --prefix=/pfs/nobackup/home/e/erikh/programs/global-array-jan2018/build``` 
* use ```make``` and next ```make install```

Remember to save the following (it is printed to the screen during compilation and will be important for setting the LD_LIBRARY_PATH and LD_RUN_PATH when compiling MOLCAS wiith GA). I often save it in a file colled for_molcas or similar.
```
----------------------------------------------------------------------
Libraries have been installed in:
   /pfs/nobackup/home/e/erikh/programs/global-array-jan2018/build/lib

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the '-LLIBDIR'
flag during linking and do at least one of the following:
   - add LIBDIR to the 'LD_LIBRARY_PATH' environment variable
     during execution
   - add LIBDIR to the 'LD_RUN_PATH' environment variable
     during linking
   - use the '-Wl,-rpath -Wl,LIBDIR' linker flag
   - have your system administrator add LIBDIR to '/etc/ld.so.conf'

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------
```

*MOLCAS install*

* Follow the guide in 

### More on DMRG calculations (should be updated!)

* Measures (entropy and mutual infomation)
  * The scripts for obtaining the mutual information can be found in
  ```
  path_to_molcas_build-dir/qcmaquis/lib/python/pyeval/
  ```
  * They are run by command (example with entropy and mutual information):
  ```
  ./mutinf.py h2o.results_state.0.h5    
  ```
  ```
  ./entropy.py h2o.results_state.0.h5
  ```

* Order of orbitals: In DMRG calculations, the order of the orbitals can be important. Usually, a good order and default start guess for the environment lattice is better than a lousy ordering and a good start guess for the environment lattice. The order according to mutual information is found by:
```fielder.py
```

### Setup of MOLCAS driver

1. After a successful build, go to molcas-root-dir/sbin and copy the molcas.driver to home/user/bin/
1. export the path: export PATH=$PATH:~/bin/ or export PATH=$PATH:home/user/bin/
1. export the explicit molcas e.g. export MOLCAS=/home/user/molcas-root-dir/build-dir/ (e.g. export MOLCAS=/home/erikh/Programs/molcas-march2017/build-gcc-mkl/)
1. 2 and 3. above can also be put into a runscript, to only set it in connection with a calculation.

### Some tips and tricks

**compiling clean-up**

```make distclean # other options are make clean; make varyclean; make extraclean```
* Fist check echo $PATH
* Looking for paths to compilers: Look in file "Symbols" in $MOLCAS directory. This file can also be modified
* Check also folder cfg (compiler flags)

**Known problems**
* For some clusters (e.g. kebnekaise) the PATH to home/user/bin needs to be defined (e.g. in .bash_profile)
```
export PATH=$HOME/bin:$PATH
```

---

### Install MOLCAS (with QCMaquis DMRG programs - depreciated, should be updated!)

#### Prerequisites (for QCMaquis)

1. MKL and Cmake
1. HDF5 ```sudo yum install hdf5``` ```sudo yum install hdf5-devel```
1. Python ```yum install python-devel.x86_64``` ```sudo yum install numpy sudo yum install scipy```
1. gsl ```yum install gsl-devel```

#### Clone and compile MOLCAS with QCMaquis

**Note:** QCMaquis depreciated. Has been moved to other repo.

1. Clone git clone git@tc-gitlab.ethz.ch:molcas-dev/fde-dmrg.git molcas
1. go to molcas root dir (path-to-molcas in the clone above)
1. submodules git submodule update --init --recursive External/gen1int-molcaslib External/hdf5_f2003_interface External/libmsym External/qcmaquis_driver External/qcmaquis_suite
1. Change CMakeList.txt: set(CMAKE_DISABLE_SOURCE_CHANGES ON) should be changed to set(CMAKE_DISABLE_SOURCE_CHANGES OFF)
1. make build-dir and enter build-dir (cd build-dir)
1. remember to set path to MKL (typically something like source /opt/intel/mkl/bin/mklvars.sh intel64)
1. FC=gfortran CC=gcc CXX=g++ cmake -DFDE=ON -DOPENMP=ON -DDMRG=ON -DLINALG=MKL -DCMAKE_BUILD_TYPE:String=RelWithDebInfo -DGEN1INT=ON ..
1. source /home/erikh/Programs/molcas-march2017/build-gcc-mkl/qcmaquis/bin/qcmaquis.sh
1. test: molcas verify qcmaquis:001 (if molcas variable is not set, see next see next session)


### QCMaquis stand-alone

** Installation:**
* Pre-requirement:
  * GSL (latest version is 2.1): Download latest stable version (gsl.tar). Unpack. 
  * HDF5 Download (here for pre-combiled) hdf5.tar.gz for the wanted compiler. Set environment variables (in .bashrc or elsewhere). ```export HDF5_DIR=/full_path_to_hdf5/```
