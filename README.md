![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

# Erik Hedegård Group Page

On this page you find tutorials and walk-throughs for the methods employed and developed in my group. 

**Research interest:**
On this page you find tutorials and walk-throughs for the methods employed and developed in my group. A short (bullet-form) description of my research interests is given below (see below or press the links for more information):

- [Theoretical modelling of biochemical and solvated molecular systems](#theoretical-modelling-of-biochemical-and-solvated-molecular-systems)
- [Polarizable embedding methods](#polarizable-embedding-methods)
- [Multiconfigurational quantum chemical methods](#multiconfigurational-quantum-chemical-methods)
- [Response theory](#response-theory)
- [Relativistic quantum chemistry](#relativistic-quantum-chemistry)


Most chemistry, including all processes relevant for living organisms, is carried out in a solvent. Our theoretical tools should therefore include this solvent. Yet the solvent is often ignored in theoretical models, since its' inclusion becomes too costly from a computational point of view. We are particularly interested in bio-chemical systems that facilitate bio-chemical transformations in living organisms. These systems are called enzymes and can be viewed as "specialized solvents". Enzymes poses the same challenges (although often much worse) than traditional solvents.

We continuously strive to improve our theoretical models to describe chemistry and spectroscopy of molecules in solution and enzyme reaction centers. Our three goals are 1) to ensure that solute (or protein reaction center) is described sufficiently accurate. 2) to ensure that all interactions between solute (or protein reaction center) are accounted for. 3) to ensure dynamics of the systems are included. Unfortunately, limitations in computer resources often generate conflicts between these three goals. For instance, quantum chemical methods can describe both a solute as well as interactions between a solute and the solvent highly accurately. Yet, these methods are also highly resource demanding and proper sampling of the dynamics is hard to achieve if we insist on describing everything by quantum chemical models. This conflict is particularly pertinent for transition metals, where the demands for an accurate quantum chemical model are even higher.

## Theoretical modelling of biochemical and solvated molecular systems

In my group we study chemistry in solution and of protein reaction centers. To carry out these calculations we combine quantum chemical methods with classical models. Thereby it becomes possible to describe the solute and solvent dynamics. 

Some of the most challenging enzymes are the ones that requires transition metals to carry out their primary function. Unfortunately, this amounts to one-third of all known enzymes, and the metalloenzymes facilitate fascinating chemical transformations, being involved in
processes ranging from metabolism of pharmaceuticals to production of fertilizers and break-down of plant material for biofuel production. In my group, we study transition metals in living organisms and in solvents. This includes development of more accurate combinations of quantum mechanics and classical models (see [Polarizable embedding methods](#polarizable-embedding-methods)). Moreover, it also includes deriving and implementing new quantum mechanical methods (see [Multiconfigurational quantum chemical methods](#multiconfigurational-quantum-chemical-methods) and [Relativistic quantum chemistry](#relativistic-quantum-chemistry)). 

**Relevant papers:**
1. Lytic polysaccharide monoxygenase: a metalloenzyme involved in bio-fuel production: [Hedegård, Ryde, Chem. Sci., 2018, 9, 3866]
2. [NiFe]-hydrogenase: enzymes for clean energy production (through hydrogen production and storage): [Geng et al. Phys. Chem. Chem. Phys., 2018, 20, 794]
3. [Fe]-hydrogenase: mechanism for the newest member of the hydrogenase family: [Hedegård et al. Angew. Chem. Int. Ed., 2015, 54, 6069]

![Local Image](<cover.jpg>)*Our latest suggestion for the molecular mechanism of metalloenzymes involved in biofuel production. From [Hedegård, Ryde, Chem. Sci., 2018, 9, 3866]*

## Polarizable embedding methods 

To facilitate accurate modeling of both solvents and proteins, we participate in the development of a polarizable embedding model. The model is designed to be employed in combination with [Response theory](#response-theory), which allow us to also model magnetic and electric spectroscopy of proteins as well as solvated systems. We have developed the polarizable embedding model in combination with both [Multiconfigurational quantum chemical methods](#multiconfigurational-quantum-chemical-methods) and [Relativistic quantum chemistry](#relativistic-quantum-chemistry)).

**Relevant papers:**
1. A multiconfigurational polarizable embedding model (including response theory): [Hedegård et al. J. Chem. Phys., 2013, 139, 044101] and [Hedegård et al. J. Chem. Phys., 2015, 142, 114113]
2. A polarizable embedding density matrix renormalization group model: [Hedegård, Reiher, J. Chem. Theory Comput., 2016, 12, 4242]

## Multiconfigurational quantum chemical methods

Certain molecular systems have a very dense manifold of states close to the ground-state. Transition metal complexes are notorious examples of this behaviour. Unfortunately, it makes most quantum chemical methods prone to failure for transition metals. Multiconfiguraitonal methods can handle a dense manifold of states is, but they are computationally expensive. This makes description of metalloenzymes even more demanding than enzymes without metals.
In my group we use multiconfigurational methods, particularly when we investigate metalloenzymes. We also develop novel multiconfiguraitonal methods, aiming to enhance the computational efficiency. This includes combining the methods with [Polarizable embedding methods](#polarizable-embedding-methods); without this combination it is close to imposible to describe a metalloenzyme. Moreover, to understand the spectroscopy of a given enzyme (which is a key ingredient to describe its mechanism), we develop multiconfigurational [Response theory](#response-theory).

**Relevant papers**
1. A short-range density functional theory combination with a multiconfigurational wave function (MC-srDFT) for open shell systems: [Hedegård et al. J. Chem. Phys., 2018, 148, 214103] 
2. Density matrix renormalization group with efficient dynamical correlation from density functional theory: [Hedegård et al. J. Chem. Phys., 142, 2015, 224108]
3. See further relevant papers in the sections on [Polarizable embedding methods](#polarizable-embedding-methods) and [Relativistic quantum chemistry](#relativistic-quantum-chemistry)

## Response theory

Spectroscopy is a tool that allow us to directly investigate the electronic structure of molecules. In cases where a molecule is too reactive to be isolated, spectroscopy is often the only way to probe the molecules’ chemical nature. Further, spectroscopy can provide structural information on chemical species in solution. 

Interpretation of spectroscopic measurements can be greatly facilitated by theoretical methods. Response theory is a theoretical framework for modeling both electronic and magnetic spectrsocopy.

Examples of spectroscopy that are applied all over the field of chemistry includes UV-vis, electric and magnetic circular dichroism (ECD and MCD), electron paramagnetic resonance (EPR), and nuclear magnetic resonance (NMR). 
Core-spectroscopies is another branch of spectroscopies; here high-energy light (often generated in synchrotrons) is employed to excite core electrons. This is highly-element specific and can be used to investigate the nature of transition metals in bio-molecular systems. 
In my group we use respones theory for bio-chemical systems and species in solution. Moreover, we develop new response models for [Polarizable embedding methods](#polarizable-embedding-methods). This allow us to model the the effect a solvent or a bio-chemical systems has on the calculated spectra.

**Relevant papers**
1. Benchmarks for excited states with range-separated hybrids between density functional theory and multiconfigurational wave functions: [Hubert et al. J. Chem. Theory Comput., 2016, 12, 2203], [Hedegård, Mol. Phys., 2017, 115, 26] and [Hubert et al. J. Phys. Chem. A, 2016, 120, 36]
2. Multiconfigurational response theory for modeling the permanganate absorption (UV-vis) spectrum in gas-phase and solution: [Olsen, Hedegård, Phys. Chem. Chem. Phys., 2017, 19, 15870]
3. See relevant sections for papers on response theory with [Multiconfigurational quantum chemical methods](#multiconfigurational-quantum-chemical-methods), [Relativistic quantum chemistry](#relativistic-quantum-chemistry) and [Polarizable embedding methods](#polarizable-embedding-methods)

![Local Image](<mad_srdft_2.jpg>)*Mean avarage deviations (MAD) in calculated excitation energies over a large benchmark set of organic molecules. The figure compare MC-srDFT with a complete active space (CAS) wave function to a number of other wave function methods. From [Hubert et al. J. Chem. Theory Comput., 2016, 12, 2203]*

## Relativistic quantum chemistry

The theory of special relativity also influences molecular systems. While the effect is usually small for valence electrons of organic compounds, the effect can become very large for transition metals. When it comes to core spectroscopy, the effect is almost always large. In my group we mainly employ and develop relativistic quantum chemical studying the spectroscopy of solvated chemical systems. This is achieved by employing [Polarizable embedding methods](#polarizable-embedding-methods)

**Relevant papers**
1. Mössbauer spectroscopy on intermediates from [Fe]-hydrogenase enzymes: [Hedegård Phys. Chem. Chem. Phys., 2014,16, 4853]
2. A relativistic polarizable embedding model: [Hedegård et al. J. Chem. Theory Comput., 2017, 13, 2870] 
![Local Image](<spec_rel.jpg>)*Difference between UV-vis spectra for aqueous pertechnetate (a) and perrhenate (b), calculated with a four-component relativistic and non-relativistic polarizable embedding (denoted "M2P2"). From [Hedegård et al. J. Chem. Theory Comput., 2017, 13, 2870]*

---

[Hedegård, Ryde, Chem. Sci., 2018, 9, 3866]: https://pubs.rsc.org/en/content/articlelanding/2018/sc/c8sc00426a#!divAbstract
[Geng et al. Phys. Chem. Chem. Phys., 2018, 20, 794]: http://pubs.rsc.org/en/Content/ArticleLanding/2018/CP/C7CP06767D#!divAbstract
[Hedegård et al. Angew. Chem. Int. Ed., 2015, 54, 6069]: https://onlinelibrary.wiley.com/doi/abs/10.1002/anie.201501737
[Hedegård et al. J. Chem. Phys., 2015, 142, 114113]: https://aip.scitation.org/doi/abs/10.1063/1.4914922?journalCode=jcp
[Hedegård, Reiher, J. Chem. Theory Comput., 2016, 12, 4242]: https://pubs.acs.org/doi/abs/10.1021/acs.jctc.6b00476
[Hedegård et al. J. Chem. Phys., 2013, 139, 044101]: https://aip.scitation.org/doi/abs/10.1063/1.4811835
[Hedegård et al. J. Chem. Phys., 142, 2015, 224108]: https://aip.scitation.org/doi/abs/10.1063/1.4922295
[Hedegård et al. J. Chem. Phys., 2018, 148, 214103]: https://aip.scitation.org/doi/abs/10.1063/1.5013306
[Hubert et al. J. Chem. Theory Comput., 2016, 12, 2203]: https://pubs.acs.org/doi/abs/10.1021/acs.jctc.5b01141
[Hubert et al. J. Phys. Chem. A, 2016, 120, 36]: https://pubs.acs.org/doi/abs/10.1021/acs.jpca.5b09662
[Hedegård, Mol. Phys., 2017, 115, 26]: https://www.tandfonline.com/doi/full/10.1080/00268976.2016.1177664?scroll=top&needAccess=true
[Olsen, Hedegård, Phys. Chem. Chem. Phys., 2017, 19, 15870]: http://pubs.rsc.org/en/content/articlelanding/2017/cp/c7cp01194f/unauth#!divAbstract
[Hedegård Phys. Chem. Chem. Phys., 2014,16, 4853]: http://pubs.rsc.org/en/content/articlelanding/2014/cp/c3cp54393e/unauth#!divAbstracti
[Hedegård et al. J. Chem. Theory Comput., 2017, 13, 2870]: https://pubs.acs.org/doi/abs/10.1021/acs.jctc.7b00162
