# Visualization tools for quantum chemistry

---

**Table of Contents**

- [VMD Structures](#vmd-structures)
- [Gaussian molecular orbitals](#gaussian-molecular-orbitals)
- [Jmol structures and molecular orbitals](#jmol-structures-and-molecular-orbitals)

---

## [VMD Structures](#vmd-structures)

Useful commands and features of the powerfull VMD software (Humphrey, W., Dalke, A. and Schulten, K., "VMD - Visual Molecular Dynamics", J. Molec. Graphics, 1996, vol. 14, pp. 33-38.)
VMD can be found here: 

### starting vmd and loading structures 

* Starting vmd: Type vmd in a terminal 
* Read in pdb or xyz structure: vmd pdb_name.pdb or vmd xyz_name.xyz

### Select atoms/residues

After starting a VMD shell (see above) go to

* Graphics => Representations: The menu "Selected Atoms" can be used
* name FE selects iron atoms
* name CA (typically selects alpha-C in protiens if the "normal" pdb name for alpha carbons are used)
* within 5 of name FE (selects all atoms withn 5 Å of FE)
* resname CYS (selects cystein residues)

### Saving pdbs with different selections (examples)  

* open vmd (```vmd```)
* load the pdb file ```mol new pdb.pdb```
    
    * **Example 1. Select all** 
    * type command ```set sel1 [atomselect top "all"]```
    * Check that the number of atoms fit by command ```$sel1num```

    * **Example 2. Save pdb with selection of atoms within 5 Å of another atom, here "Mn"**
    * Type ```vmd > set sel2 [atomselect top "within 5 of name MN"]```

    * **Example 3. Select whole molecules, here waters - and only waters**
    * Type ```vmd > set sel3 [atomselect top "same residue as (water within 5 of name MN)"]```

    * **Example 4. First residues and waters within 5 Å** 
    * Type ```vmd > set sel4 [atomselect top "resid 1 or (same residue as (water within 5 of resid 1))"]```

* For all examples, write the pdb using writepdb selection: ```vmd > $selX writepdb pdb_out.pdb```

### Align molecules with VMD RMSD Tool

**Note:** this can also be done (often faster) with Maestro "Quick Align"

* Open a VMD shell (see above)
* Extensions => Analysis => RMSD Trajectory Tool. Check that it is the right molecules that are read in (try "Add all"). 
* Now type for the atoms that should be aligned (in box upper left) name ATOM1 ATOM2 ATOM3 etc. (note that they should have the same name in the pdb/xyz files that are compared. It only works if there are equally many of the atom types in the fwo files!). 
* Press "ALIGN". IMPORTANT: The align will happend according to the "top" molecule (marked with "T" in the VMD main window - this molecule will have unchanged coordinates)
* xyz or pdb file can be written out with from File => Save Coordinates.

## [Gaussian molecular orbitals](#gaussian-molecular-orbitals)

Molecular orbitals are often useful for understanding the electronic structure of a molecule. They are particularly useful for interpreting electronic spectroscopy (i.e. UV-vis spectra). 
In gaussian, the can be generated in different ways and visualized with Jmol (see below). We focus here on generation of cube files that can be visualized with Jmol (see below).

* Run a gaussian wave function optimization and make sure you obtain the ".chk" file, e.g. gaussian.chk. 
* Transform the ".chk" with the utility program `formchk gaussian.chk gaussian.fchk`. Note: make sure the gaussian module is loaded (aurora: `module load gaussian/16.B.01-AVX2`)
* Use the utility program cubegen `cubegen 0 MO=<number> gaussian.fchk <name>.cube`. Note for an unrestricted calculation, use "AMO" or "BMO" instead.

**Script to generate many MOs**
`for i in {130..165}; do cubegen 0 MO="$i" orbitals.fchk "$i".cube ; done` 

## [Jmol structures and molecular orbitals](#jmol-structures-and-molecular-orbitals)

* The following java script can be inserted in the Jmol scripting window - It loads the files rotate them (adjust this according to your needs), writes the distances between atoms 49/50, 49/1, 49/10 and 49/30 and prints a frame in jpg format.

```
background white
load FILES "o2_state_tpssd3_ooh2_opt_cosmo.xyz"
"o2_state_tpssd3_react_ooh_opt_cosmo_singlet.xyz"
"o2_state_tpssd3_prod_ooh_opt_cosmo.xyz"
"o2_state_tpssd3_react_oo_opt_cosmo.xyz" "o2_state_tpssd3_oh2_opt_cosmo.xyz" <tt>"o2_state_tpssd3_react_oh_opt_cosmo_singlet.xyz"
"o2_state_tpssd3_prod_oh_opt_cosmo.xyz" "o2_state_tpssd3_o_opt_cosmo.xyz"
    
for (i=1; i<9; i++) {
   frame @i
    zoom 85
    rotate x 148
    rotate y 75
    rotate z -92
    select all; wireframe 40; monitor 49 50; monitor 49 1; monitor 49 10; monitor 49 30;
    fname = "frame_a" + @{i} + ".jpg"
    write jpg @{fname}
    rotate z 92
    rotate y -75
    rotate x -148
}
```

### Molecular orbitals

* Start the program ```java -jar /path/jmol/Jmol.jar```. 
  Example: ```java -jar /home/erik/programs/jmol/Jmol.jar```
* Use the script 
```
background white
for (i=1; i<10; i++) {
    isosurface color yellow blue mo @{i} opaque
    fname = "orb" + @{i} + ".jpg"
    write jpg @{fname}
}
```

**From cube files** 

In the directory with the cube files, run the script `generate_orb.sh`:

```
#!/bin/bash

for i in {140..162} # This is the MO interval. Change if required...
    do
    echo "background white" > orbital_$i".spt"
    echo "load structure.xyz;spacefill 0.2" >> orbital_$i".spt"
    echo 'isosurface sign "'$i.cube'" translucent 0.4' >> orbital_$i".spt"
    echo "write jpg $i.jpg" >> orbital_$i".spt"
    jmol -ions orbital_$i.spt
    rm orbital_$i.spt
done
```

### Known problems
* **DALTON:** The molden file from dalton cannot be opened by jmol. Open the molden.inp file with molden and write out a new file (select "write" in the moden menu). The new file can be opened by jmol
